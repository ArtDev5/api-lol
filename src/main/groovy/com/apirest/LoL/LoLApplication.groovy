package com.apirest.LoL

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class LoLApplication {

	static void main(String[] args) {
		SpringApplication.run(LoLApplication, args)
	}

}
