package com.apirest.LoL.controller.error

class ApiError {

    private String errorMessage

    ApiError(String errorMessage) {
        this.errorMessage = errorMessage
    }

    String getErrorMessage() {
        return errorMessage
    }

    @Override
    public String toString() {
        return "ApiError{" +
                "errorMessage='" + errorMessage + '\'' +
                '}';
    }
}
