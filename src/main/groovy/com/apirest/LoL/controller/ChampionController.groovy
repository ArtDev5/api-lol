package com.apirest.LoL.controller

import com.apirest.LoL.controller.error.ApiError
import com.apirest.LoL.model.ChampionEntity
import com.apirest.LoL.repository.ChampionRepository
import com.apirest.LoL.repository.filter.ChampionFilter
import com.apirest.LoL.repository.filter.ChampionRecommendationFilter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/champions")
class ChampionController {

    @Autowired
    private ChampionRepository championRepository

    @GetMapping
    ResponseEntity<List<ChampionEntity>> getChampions(Pageable pageable){
        List<ChampionEntity> champions = championRepository.findAll(pageable) as List<ChampionEntity>
        return ResponseEntity.status(HttpStatus.OK).body(champions)
    }

    @PostMapping
    ResponseEntity<ChampionEntity> createChampion(@RequestBody ChampionEntity championEntity){
        championRepository.save(championEntity)
        return ResponseEntity.status(HttpStatus.CREATED).body(championEntity)
    }

    @PutMapping("/{id}")
    ResponseEntity<ChampionEntity> updateChampion(@PathVariable Long id, @RequestBody ChampionEntity championEntity){
        Optional<ChampionEntity> optionalChampion = championRepository.findById(id)

        if(optionalChampion.isPresent()){
            ChampionEntity newChampion = championEntity
            newChampion.setId(id)
            championRepository.save(newChampion)
            return ResponseEntity.status(HttpStatus.OK).body(newChampion)
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body()
    }

    @DeleteMapping("/{id}")
    ResponseEntity deleteChampion(@PathVariable Long id){
        Optional<ChampionEntity> optionalChampion = championRepository.findById(id)

        if(optionalChampion.isPresent()){
            championRepository.deleteById(id)
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body()
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body()
    }

    @PostMapping("/specific")
    ResponseEntity<List<ChampionEntity>> getSpecificChampions(@RequestBody ChampionFilter championFilter){
        String name = championFilter.getName()

//        if(name != null && name.length() < 3){
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiError("Mínimo 3 " +
//                    "caracteres"))
//        }

        List<ChampionEntity> champions = championRepository.filterChampion(championFilter)

        return ResponseEntity.status(HttpStatus.OK).body(champions)
    }

    @PostMapping("/recommendation")
    ResponseEntity getChampionsRecommendation(@RequestBody ChampionRecommendationFilter recommendationFilter){
        int numberOfParameters = recommendationFilter.getNumberOfParameters()

        if(numberOfParameters < 2){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiError("Mínimo 2 " +
                    "parâmetros"))
        }

        List<ChampionEntity> champions = championRepository.filterChampionRecommendation(recommendationFilter)

        return ResponseEntity.status(HttpStatus.OK).body(champions)
    }

}
