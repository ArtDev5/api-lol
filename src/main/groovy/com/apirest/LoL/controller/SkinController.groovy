package com.apirest.LoL.controller

import com.apirest.LoL.model.ChampionEntity
import com.apirest.LoL.model.SkinEntity
import com.apirest.LoL.repository.ChampionRepository
import com.apirest.LoL.repository.SkinRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.util.StringUtils
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/skins")
class SkinController {

    @Autowired
    private ChampionRepository championRepository

    @Autowired
    private SkinRepository skinRepository

    @PostMapping
    ResponseEntity<SkinEntity> createSkin(@RequestBody SkinEntity skinEntity){
        String championName = skinEntity.getChampionName()
        Optional<ChampionEntity> optionalChampion = championRepository.findByName(championName)

        if(optionalChampion.isPresent()){
            skinRepository.save(skinEntity)
            return ResponseEntity.status(HttpStatus.CREATED).body(skinEntity)
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body()
    }

    @DeleteMapping("/{id}")
    ResponseEntity deleteSkin(@PathVariable Long id){
        Optional<SkinEntity> optionalSkin = skinRepository.findById(id)

        if(optionalSkin.isPresent()){
            skinRepository.deleteById(id)
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body()
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body()
    }

    @PutMapping("/{id}")
    ResponseEntity<SkinEntity> updateSkin(@PathVariable Long id, @RequestBody SkinEntity skinEntity){
        Optional<SkinEntity> optionalSkin = skinRepository.findById(id)

        if(optionalSkin.isPresent()){
            SkinEntity newSkin = skinEntity
            newSkin.setId(id)
            skinRepository.save(newSkin)
            return ResponseEntity.status(HttpStatus.OK).body(newSkin)
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body()
    }

    @GetMapping("/{championName}")
    ResponseEntity<List<SkinEntity>> getSkin(@PathVariable String championName){
        String name = championName.toLowerCase()
        String nameWithUppercase = StringUtils.capitalize(name)

        List<SkinEntity> skins = skinRepository.findByChampionName(nameWithUppercase)

        if(!skins.isEmpty()) {
            return ResponseEntity.status(HttpStatus.OK).body(skins)
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body()
    }
}
