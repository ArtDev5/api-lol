package com.apirest.LoL.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table
import javax.validation.constraints.NotEmpty

@Entity
@Table(name = "skins")
class SkinEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id
    @NotEmpty
    private String name
    @NotEmpty
    private String championName

    SkinEntity(Long id, String name, String championName) {
        this.id = id
        this.name = name
        this.championName = championName
    }

    SkinEntity(String name, String championName) {
        this.name = name
        this.championName = championName
    }

    SkinEntity() {
    }

    Long getId() {
        return id
    }

    void setId(Long id) {
        this.id = id
    }

    String getName() {
        return name
    }

    void setName(String name) {
        this.name = name
    }

    String getChampionName() {
        return championName
    }

    void setChampionName(String championName) {
        this.championName = championName
    }

    @Override
    public String toString() {
        return "SkinEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", championName='" + championName + '\'' +
                '}';
    }
}
