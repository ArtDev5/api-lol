package com.apirest.LoL.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table
import javax.validation.constraints.NotEmpty

@Entity
@Table(name = "champions")
class ChampionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id
    @NotEmpty
    private String name
    @NotEmpty
    private String title
    @NotEmpty
    private String role
    @NotEmpty
    private String difficulty
    @NotEmpty
    private String region

    ChampionEntity() {
    }

    ChampionEntity(Long id, String name, String title, String role, String difficulty, String region) {
        this.id = id
        this.name = name
        this.title = title
        this.role = role
        this.difficulty = difficulty
        this.region = region
    }

    ChampionEntity(String name, String title, String role, String difficulty, String region) {
        this.name = name
        this.title = title
        this.role = role
        this.difficulty = difficulty
        this.region = region
    }

    Long getId() {
        return id
    }

    void setId(Long id) {
        this.id = id
    }

    String getName() {
        return name
    }

    void setName(String name) {
        this.name = name
    }

    String getTitle() {
        return title
    }

    void setTitle(String title) {
        this.title = title
    }

    String getRole() {
        return role
    }

    void setRole(String role) {
        this.role = role
    }

    String getDifficulty() {
        return difficulty
    }

    void setDifficulty(String difficulty) {
        this.difficulty = difficulty
    }

    String getRegion() {
        return region
    }

    void setRegion(String region) {
        this.region = region
    }


    @Override
    public String toString() {
        return "ChampionEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", title='" + title + '\'' +
                ", role='" + role + '\'' +
                ", difficulty='" + difficulty + '\'' +
                ", region='" + region + '\'' +
                '}';
    }
}
