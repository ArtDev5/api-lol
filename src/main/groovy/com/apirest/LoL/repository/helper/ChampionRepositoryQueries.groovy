package com.apirest.LoL.repository.helper

import com.apirest.LoL.model.ChampionEntity
import com.apirest.LoL.repository.filter.ChampionFilter
import com.apirest.LoL.repository.filter.ChampionRecommendationFilter

interface ChampionRepositoryQueries {
    List<ChampionEntity> filterChampion(ChampionFilter championFilter)
    List<ChampionEntity> filterChampionRecommendation(ChampionRecommendationFilter recommendationFilter)
}