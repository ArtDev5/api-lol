package com.apirest.LoL.repository.helper

import com.apirest.LoL.model.ChampionEntity
import com.apirest.LoL.repository.filter.ChampionFilter
import com.apirest.LoL.repository.filter.ChampionRecommendationFilter

import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.Query

class ChampionRepositoryImpl implements ChampionRepositoryQueries{

    @PersistenceContext
    private EntityManager manager

    @Override
    List<ChampionEntity> filterChampion(ChampionFilter championFilter){
        StringBuilder sb = new StringBuilder()

        String name = championFilter.getName()
        Long id = championFilter.getId()

        if (name != null && id != null){
            sb.append(" SELECT bean FROM ChampionEntity bean WHERE name like '%" + name + "%' and id = " + id)
        } else if (name != null){
            sb.append(" SELECT bean FROM ChampionEntity bean WHERE name like '%" + name + "%'")
        } else if (id != null){
            sb.append(" SELECT bean FROM ChampionEntity bean WHERE id = " + id )
        }

        Query query = manager.createQuery(sb.toString(), ChampionEntity.class);

        return query.getResultList();
    }

    @Override
    List<ChampionEntity> filterChampionRecommendation(ChampionRecommendationFilter recommendationFilter) {
        StringBuilder sb = new StringBuilder()

        String region = recommendationFilter.getRegion()
        String difficulty = recommendationFilter.getDifficulty()
        String role = recommendationFilter.getRole()

        String sqlRegion = (region != null) ? "= '"+ region + "'" : "is not null"
        String sqlDifficulty = (difficulty != null) ? "= '" + difficulty + "'" : "is not null"
        String sqlRole = (role != null) ? "= '" + role + "'" : "is not null"

        sb.append(" SELECT bean FROM ChampionEntity bean WHERE region " + sqlRegion + " and difficulty " +
                sqlDifficulty + " and role " + sqlRole)

        Query query = manager.createQuery(sb.toString(), ChampionEntity.class);

        return query.getResultList();
    }
}
