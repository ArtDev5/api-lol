package com.apirest.LoL.repository

import com.apirest.LoL.model.SkinEntity
import org.springframework.data.jpa.repository.JpaRepository

interface SkinRepository extends JpaRepository<SkinEntity, Long>{
    List<SkinEntity> findByChampionName(String championName)
}