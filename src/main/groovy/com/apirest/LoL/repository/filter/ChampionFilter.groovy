package com.apirest.LoL.repository.filter

class ChampionFilter {

    Long id
    String name

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        ChampionFilter that = (ChampionFilter) o

        if (id != that.id) return false
        if (name != that.name) return false

        return true
    }

    int hashCode() {
        int result
        result = (id != null ? id.hashCode() : 0)
        result = 31 * result + (name != null ? name.hashCode() : 0)
        return result
    }
}
