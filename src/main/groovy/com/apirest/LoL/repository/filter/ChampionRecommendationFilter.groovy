package com.apirest.LoL.repository.filter

class ChampionRecommendationFilter {

    private String role
    private String difficulty
    private String region

    String getRole() {
        return role
    }

    void setRole(String role) {
        this.role = role
    }

    String getDifficulty() {
        return difficulty
    }

    void setDifficulty(String difficulty) {
        this.difficulty = difficulty
    }

    String getRegion() {
        return region
    }

    void setRegion(String region) {
        this.region = region
    }

    Integer getNumberOfParameters(){
        Integer numberOfParameters = 0

        if(getDifficulty() != null){
            numberOfParameters++
        }
        if(getRegion() != null){
            numberOfParameters++
        }
        if(getRole() != null){
            numberOfParameters++
        }
        return numberOfParameters
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        ChampionRecommendationFilter that = (ChampionRecommendationFilter) o

        if (difficulty != that.difficulty) return false
        if (region != that.region) return false
        if (role != that.role) return false

        return true
    }

    int hashCode() {
        int result
        result = (role != null ? role.hashCode() : 0)
        result = 31 * result + (difficulty != null ? difficulty.hashCode() : 0)
        result = 31 * result + (region != null ? region.hashCode() : 0)
        return result
    }
}
