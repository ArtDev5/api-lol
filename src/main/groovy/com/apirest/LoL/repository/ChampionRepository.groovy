package com.apirest.LoL.repository

import com.apirest.LoL.model.ChampionEntity
import com.apirest.LoL.repository.helper.ChampionRepositoryQueries
import org.springframework.data.jpa.repository.JpaRepository

interface ChampionRepository extends JpaRepository<ChampionEntity, Long>, ChampionRepositoryQueries{
    Optional<ChampionEntity> findByName(String name)
}