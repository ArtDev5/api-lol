package com.apirest.LoL.controller

import com.apirest.LoL.model.ChampionEntity
import com.apirest.LoL.model.SkinEntity
import com.apirest.LoL.repository.ChampionRepository
import com.apirest.LoL.repository.SkinRepository
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

import static org.hamcrest.Matchers.hasSize
import static org.hamcrest.Matchers.is
import static org.mockito.Mockito.verify
import static org.mockito.Mockito.when

@SpringBootTest
@AutoConfigureMockMvc
class SkinControllerTests {

    @Autowired
    private MockMvc mockMvc
    @Autowired
    private ObjectMapper objectMapper
    @MockBean
    private ChampionRepository championRepository
    @MockBean
    private SkinRepository skinRepository

    @Test
    void "should return a status code 201 and save a skin in post method"() {

        ChampionEntity championEntity = new ChampionEntity("Ahri", "A raposa de nove caudas", "Mago",
                "Moderado", "Ionia")

        when(championRepository.findByName("Ahri")) thenReturn(Optional.of(championEntity))

        SkinEntity skinEntity = new SkinEntity(1, "Ahri colegial", "Ahri")

        when(skinRepository.save(skinEntity)) thenReturn(skinEntity)

        mockMvc.perform(MockMvcRequestBuilders.post("/skins")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(skinEntity)))
                .andExpect(MockMvcResultMatchers.jsonPath('id', is(1)))
                .andExpect(MockMvcResultMatchers.status().isCreated())
    }

    @Test
    void "Should return status code 204 and delete a skin in delete method"(){
        SkinEntity skinEntity = new SkinEntity("Ahri colegial", "Ahri")

        when(skinRepository.findById(1))thenReturn(Optional.of(skinEntity))

        mockMvc.perform(MockMvcRequestBuilders.delete("/skins/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent())

        verify(skinRepository).deleteById(1)
    }

    @Test
    void "Should return status code 200 and update a skin in put method"(){
        SkinEntity skinEntity = new SkinEntity("Ahri colegiallllllll", "Ahri")

        when(skinRepository.findById(1))thenReturn(Optional.of(skinEntity))

        mockMvc.perform(MockMvcRequestBuilders.put("/skins/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(skinEntity)))
                .andExpect(MockMvcResultMatchers.jsonPath('name', is("Ahri colegiallllllll")))
                .andExpect(MockMvcResultMatchers.status().isOk())
    }

    @Test
    void "Should return status code 200 and 1 element in get method"(){
        String championName = "Ahri"

        when(skinRepository.findByChampionName(championName)).thenReturn(Arrays.asList(
                new SkinEntity("Ahri colegial", "Ahri")
        ))

        mockMvc.perform(MockMvcRequestBuilders.get("/skins/{championName}", championName)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath('$', hasSize(1)))
    }
}