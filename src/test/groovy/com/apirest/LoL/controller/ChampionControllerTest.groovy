package com.apirest.LoL.controller

import com.apirest.LoL.model.ChampionEntity
import com.apirest.LoL.repository.ChampionRepository
import com.apirest.LoL.repository.filter.ChampionFilter
import com.apirest.LoL.repository.filter.ChampionRecommendationFilter
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

import static org.hamcrest.Matchers.hasSize
import static org.hamcrest.Matchers.is
import static org.mockito.ArgumentMatchers.any
import static org.mockito.Mockito.when

@SpringBootTest
@AutoConfigureMockMvc
class ChampionControllerTest {

    @Autowired
    private MockMvc mockMvc
    @MockBean
    private ChampionRepository championRepository
    @Autowired
    private ObjectMapper objectMapper

    @Test
    void "Should return status code 200 and 1 element in get method"(){

        when(championRepository.findAll()).thenReturn(Arrays.asList(
                new ChampionEntity("Ahri", "A raposa de nove caudas", "Mago", "Moderado",
                        "Ionia")))

        mockMvc.perform(MockMvcRequestBuilders.get("/champions")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath('$',hasSize(1)))
    }

    @Test
    void "Should return status code 201 in post method and save a champion"(){
        ChampionEntity championEntity = new ChampionEntity(1,"Ahri", "A raposa de nove caudas", "Mago",
                "Moderado", "Ionia")

        when(championRepository.save(championEntity)).thenReturn(championEntity)

        mockMvc.perform(MockMvcRequestBuilders.post("/champions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(championEntity)))
                .andExpect(MockMvcResultMatchers.jsonPath("id", is(1)))
                .andExpect(MockMvcResultMatchers.status().isCreated())
    }

    @Test
    void "Should return status code 200 and update a champion in put method"(){
        ChampionEntity championEntity = new ChampionEntity("Ahri", "A raposa de nove caudas", "Mago",
                "Moderado", "Ionia")

        when(championRepository.findById(1))thenReturn(Optional.of(championEntity))

        mockMvc.perform(MockMvcRequestBuilders.put("/champions/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(championEntity)))
                .andExpect(MockMvcResultMatchers.jsonPath("name", is("Ahri")))
                .andExpect(MockMvcResultMatchers.status().isOk())
    }

    @Test
    void "Should return status code 204 and delete a champion in delete method"(){
        ChampionEntity championEntity = new ChampionEntity("Ahri", "A raposa de nove caudas", "Mago",
                "Moderado", "Ionia")

        when(championRepository.findById(1))thenReturn(Optional.of(championEntity))

        mockMvc.perform(MockMvcRequestBuilders.delete("/champions/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent())
    }

    @Test
    void "should return a status code 400 when input less than three characters"(){
        ChampionFilter championFilter = new ChampionFilter()
        championFilter.setName("ri")

        mockMvc.perform(MockMvcRequestBuilders.post("/champions/specific")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(championFilter)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())

    }

    @Test
    void "Should return champions that contain the parameters passed"(){
        ChampionFilter championFilter = new ChampionFilter()
        championFilter.setName("hri")

        when(championRepository.filterChampion(championFilter)).thenReturn(Arrays.asList(
                new ChampionEntity("Ahri", "A raposa de nove caudas", "Mago", "Moderado",
                        "Ionia")))

        mockMvc.perform(MockMvcRequestBuilders.post("/champions/specific")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(championFilter)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath('$',hasSize(1)))
    }

    @Test
    void "should return a status code 400 when input less than two parameters"(){
        ChampionRecommendationFilter recommendationFilter = new ChampionRecommendationFilter()
        recommendationFilter.setRole("Mago")

        mockMvc.perform(MockMvcRequestBuilders.post("/champions/recommendation")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(recommendationFilter)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
    }

    @Test
    void "Should return recommended champions that contain the parameters passed"(){
        ChampionRecommendationFilter recommendationFilter = new ChampionRecommendationFilter()
        recommendationFilter.setRegion("Ionia")
        recommendationFilter.setRole("Mago")

        when(championRepository.filterChampionRecommendation(recommendationFilter)).thenReturn(Arrays.asList(
                new ChampionEntity("Ahri", "A raposa de nove caudas", "Mago", "Moderado",
                        "Ionia")))

        mockMvc.perform(MockMvcRequestBuilders.post("/champions/recommendation")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(recommendationFilter)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath('$',hasSize(1)))
    }
}
